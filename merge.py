# make sure you have a kerberos ticket
from apd import AnalysisData
import subprocess
import argparse 
from ROOT import RDataFrame
from ROOT import TFile, TChain, TTree
import numpy as np

def find_all_trees(file):
    first = TFile.Open(file)
    trees = first.GetListOfKeys()

    def iterate_trees(obj, path=""):
        if isinstance(obj, TTree):
            return [path  + ("/" if path != "" else "") + obj.GetName()]
        else:
            return [tree for key in obj.GetListOfKeys() for tree in iterate_trees(key.ReadObj(), path + ("/" if path != "" else "") + obj.GetName())]
    
    return np.array([iterate_trees(tree.ReadObj()) for tree in trees],dtype=object).flatten()
    
def get_dir(tree_path):
    return "/".join(tree_path.split("/")[:-1])

def merge_files(files, output_file):
    if len(files) == 0:
        raise ValueError("No files to merge")
    
    trees = find_all_trees(files[0])

    chains = {tree:TChain(tree) for tree in trees}
    for file in files:
        for tree in trees:
            chains[tree].Add(file)
    dirs = set([get_dir(tree) for tree in trees])

    output = TFile(output_file, "RECREATE")
    dir_dict = {dir:output.mkdir(dir) for dir in dirs}

    for tree in trees:
        dir_dict[get_dir(tree)].cd()
        chains[tree].CloneTree(-1, "")
    output.Write()
    output.Close()




def main():
    parser = argparse.ArgumentParser(description="Script to merge & transfer Analysis Production output files to eospublic")

    # Define named arguments
    parser.add_argument('--path', type=str, help='Output path on eospublic')
    parser.add_argument('--name', type=str, help='Analysis Name')
    parser.add_argument('--working_group', type=str, default="dpa", help='working group')
    parser.add_argument("--file_system", type=str, default="root://eospublic.cern.ch/", help="File system where the files are stored (default: root://eospublic.cern.ch/)")
    parser.add_argument("--limit", type=float, default=3.0, help="Maximum size of the output files in GB (default: 1.5)")
    # Parse command-line arguments
    args = parser.parse_args()
    # Access named arguments
    working_group = args.working_group
    ana_name = args.name
    path = args.path
    limit = args.limit
    datasets = AnalysisData(working_group, ana_name)
    years = sorted(list(datasets.summary()['tags']['datatype']))
    for year in years:
        for p in ["magup", "magdown"]:
            pfn_year = datasets(datatype=year, polarity=p)
            
            sizes = [TFile.Open(file).GetSize()/1e9 for file in pfn_year] # in GB
            cumsum = np.cumsum(sizes)
            indices_to_Split = []
            limit_ = limit
            for i, size in enumerate(cumsum):
                if i == 0:
                    continue
                if size > limit_:
                    indices_to_Split.append(i)
                    limit_ = limit_ + limit
            size_observing_sets = np.split(pfn_year, indices_to_Split)

            for i, file_list in enumerate(size_observing_sets):
                print(f"Processing {ana_name}_{year}_{p}_{i}.root")
                file_name =f"{ana_name}_{year}_{p}_{i}.root"
                merge_files(file_list, f"{args.file_system}{path}/{file_name}")

if __name__ == "__main__":
    main()